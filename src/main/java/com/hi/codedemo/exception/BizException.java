package com.hi.codedemo.exception;


import com.hi.codedemo.code.define.MsgCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 自定义业务异常类
 *
 * @author cuik
 * @date 2020/12/25 17:28
 * @since 1.0.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BizException extends RuntimeException {


    private String code;

    private String msg;

    private Object data;


    private BizException(String code) {
        this.code = code;
    }

    public BizException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * @param message 错误信息
     * @return BizException
     */
    public static BizException buildWithMsg(String message) {
        return new BizException(null, message, null);
    }

    /**
     * @param code 错误码
     * @return BizException
     */
    public static BizException build(String code) {
        return new BizException(code);
    }

    /**
     * @param code 错误码
     * @param msg  描述
     * @return BizException
     */
    public static BizException build(String code, String msg) {
        return new BizException(code, msg, null);
    }

    public static BizException build(MsgCode errMsgEnum) {
        return new BizException(errMsgEnum.getCode(), errMsgEnum.getMsg(), null);
    }


}
