package com.hi.codedemo.code.define;

/**
 * code属性定义
 */
class Code {
    private String code;
    private String msg;

    String getCode() {
        return code;
    }

    void setCode(String code) {
        this.code = code;
    }

    String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
