package com.hi.codedemo.code.define;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 获取code相关注解实现
 */
class CodeRepository {
    private static final Map<Object, Code> CODE_MAP = new ConcurrentHashMap<>();

    /**
     *     数字编号补全
     */
    private static final String LEFT_PAD = System.getProperty("hi.common.msgCode.leftPad", "4");

    static Code get(Object object) {
        if (CODE_MAP.containsKey(object)) {
            return CODE_MAP.get(object);
        }
        try {
            Enum<?> codeEnum = (Enum<?>) object;
            CodePrefix codePrefix = object.getClass().getAnnotation(CodePrefix.class);
            Cc c = object.getClass().getField(codeEnum.name()).getAnnotation(Cc.class);
            Code code = new Code();
            int cod = c != null ? c.value() : codeEnum.ordinal();
            code.setCode(codePrefix != null ? String.format("%s%0"+LEFT_PAD+"d", codePrefix.value(), cod)
                    : String.valueOf(cod));
            code.setMsg(c != null && c.msg().length() > 0 ? c.msg() : codeEnum.name());
            CODE_MAP.put(object,code);
            return code;
        } catch (NoSuchFieldException e) {
            throw new Error(e);
        }
    }
}
