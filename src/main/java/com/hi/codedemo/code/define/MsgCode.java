package com.hi.codedemo.code.define;


/**
 *
 */
public interface MsgCode {
    /**
     * 获取消息
     *
     * @return msg
     */
    default String getMsg() {
        return CodeRepository.get(this).getMsg();
    }

    /**
     * 获取code
     *
     * @return code
     */
    default String getCode() {
        return CodeRepository.get(this).getCode();
    }
}
