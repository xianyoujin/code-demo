package com.hi.codedemo.code;


import com.hi.codedemo.code.define.Cc;
import com.hi.codedemo.code.define.CodePrefix;
import com.hi.codedemo.code.define.MsgCode;

/**
 * 错误码定义
 */
public interface ErrorCode {
    /**
     * 公共异常
     */
    enum Common implements MsgCode {
        @Cc(value = 0,msg = "ok")
        success,
        @Cc(value = -1,msg = "unknown exception")
        fail
    }

    /**
     * 客户端异常
     * 错误来源于用户（入参），比如参数错误，用户支付超时等问题
     */
    @CodePrefix("A")
    enum Client implements MsgCode{
        @Cc(value = 1,msg = "未登陆")
        no_login,
    }

    /**
     * 系统内部相关异常
     * 错误来源于当前系统，往往是业务逻辑出错，或程序健壮性差等问题，如系统处理订单超时
     */
    @CodePrefix("B")
    enum System implements MsgCode{
        @Cc(value = 1,msg = "系统异常")
        system_error,
    }

    /**
     * 第三方服务异常(外部异常)
     * 错误来源 如第三方服务，比如 Redis连接出错，消息投递超时等问题；
     */
    @CodePrefix("C")
    enum External  implements MsgCode{
        @Cc(value = 1,msg = "redis连接超时")
        redis_timeout,
    }
}
