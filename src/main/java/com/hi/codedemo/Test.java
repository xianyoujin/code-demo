package com.hi.codedemo;


import com.hi.codedemo.code.ErrorCode;
import com.hi.codedemo.exception.BizException;

public class Test {
    public static void main(String[] args) {
        //输出ok
        System.out.println(ErrorCode.Common.success.getMsg());
        //输出B0001
        System.out.println(ErrorCode.System.system_error.getCode());
        //自定义异常构建
        BizException.build(ErrorCode.Client.no_login);
    }
}
